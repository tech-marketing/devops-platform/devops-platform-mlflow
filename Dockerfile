FROM python:3.8-slim-buster
COPY ./model.py /app/model.py
COPY ./mlruns/ /app/mlruns/
COPY  ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT [ "mlflow" ]
CMD [ "ui", "--host=0.0.0.0" ]
